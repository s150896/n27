# Klausur 3 in der Woche des 18.03.20

* evtl. Fehler finden
* Logik ausprogrammieren (if/else)
* Fehler behandeln (Programm darf nicht abstürzen, Fehler benennen)
* String verketten (innerhalb eines SQL-Statements, Anführungszeichen!)
* if/else (siehe server.js!!, auch verschachtelt)
* zur SQL-Syntax eine Aufgabe (machen wir noch, eignenen SQL-Befehl erstellen)
* Übergabe einer Variablen an ein SQL-Statement  (String-Verkettung, aus dem Request eine Variable an SQL überreichen)
* app.post und app.get (auseinanderhalten können, anlegen)
* evtl. Modul einbinden, das etwas amcht z.B. "email-validator" (einbinden und einbauen, prüft ob die E-Mail korrekt ist, keine Sonderzeichen, keine Umlaute, nicht mit Ziffer beginnen, @, . , 